const bodyParser = require('body-parser')
const express = require('express');
const helmet = require('helmet');
const dotenv = require("dotenv");
const cors = require('cors');
const app = express();
const port = process.env.PORT || 3001;

dotenv.config()

app.use(helmet())

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());


app.use(cors({
    origin: ['http://localhost:3000'],
    allowedHeaders: ['Content-Type', 'Authorization'],
    credentials: true,

}))

const stripe = require("stripe")(process.env.STRIPE_SECRET_KEY);

app.post('/create_order', async (req, res) => {
    console.log(req.body.order);
    const order = req.body.order // object with details of the order
    const source = req.body.source // the orders token id
    try {
        const stripeOrder = await stripe.orders.create(order)
        console.log(`Order created: ${stripeOrder.id}`)
        await stripe.orders.pay(stripeOrder.id, {source})
    } catch (err) {
        // Handle stripe errors here: No such coupon & sku
        console.log(`Order error: ${err}`)
        return res.sendStatus(404)
    }
    return res.sendStatus(200)
})

app.listen(port, () => console.log(`Example app listening on port ${port}!`))