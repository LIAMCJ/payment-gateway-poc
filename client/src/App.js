import React from 'react';
import './App.css';
import {StripeProvider, Elements} from 'react-stripe-elements'
import Header from './components/header'
import Shop from './components/main'

class App extends React.Component {
  render() {
    return (
        <section>
            <Header/>
            <StripeProvider apiKey="pk_test_Czq7Phc4o1lT5VKkXUkSxViN00FidvVntF">
                <Elements>
                    <Shop/>
                </Elements>
            </StripeProvider>
        </section>
    )
  }
}

export default App;