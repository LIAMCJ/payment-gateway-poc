import React from 'react';
import {products} from "./data/products";
import {productData} from './data/productData'
import { CardElement , injectStripe} from 'react-stripe-elements';
import {Form, Button, Col, Card, Container, Row, Fade } from "react-bootstrap"
import PropTypes from 'prop-types';
import axios from 'axios';

const prices = productData[0];

const skus = productData[1];

class Shop extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            display: false,
            validated: false,
            fetching: false,
            cart: {
                apple: 0,
                orange: 0
            },
            email: '',
            name: '',
            address : {
                line1: '',
                city: '',
                state: '',
                country: '',
                postal_code: ''
            }
        }

    }


    
    handleCartChange = (event) => {
        event.preventDefault()
        const cart = this.state.cart
        cart[event.target.name]+= parseInt(event.target.value)
        this.setState({cart})
    }

    handleSubmit = (event) => {
        event.preventDefault()
        const form = event.currentTarget;
        this.setState({fetching: true})
        const state = this.state
        const cart = state.cart

        if (form.checkValidity() === false) {
            event.stopPropagation();
            this.setState({validated: true})
        }else{

            this.setState({validated: true})

            this.props.stripe.createToken({name: state.name}).then(({token}) => {
                  // Create order
                  const order = {
                    currency: 'usd',
                    items: Object.keys(cart).filter((name) => cart[name] > 0 ? true : false).map(name => {
                      return {
                        type: 'sku', 
                        parent: skus[name],
                        quantity: cart[name]
                      }
                    }),
                    email: state.email,
                    shipping: {
                      name: state.name,
                      address: state.address
                    }
                  }

                  // Send order
                  axios.post(`http://localhost:3001/create_order`, {order, source: token.id})
                  .then(() => {
                    this.setState({fetching: false})
                    alert(`Thank you for your purchase!`)
                    console.log('SUCCESS');
                    this.setState({cart:{apple: 0, orange: 0}})
                  })
                  .catch(error => {
                    this.setState({fetching: false})
                    alert("ERROR: " + error)
                  })
                }).catch(error => {
                  this.setState({fetching: false})
                  alert("ERROR: " + error)
                })

                
        }
    };

    handleChange = (event) => {
        event.preventDefault()
        this.setState({[event.target.name]: event.target.value})
    }

    handleAddressChange = (event) => {
        event.preventDefault()
        const address = this.state.address
        address[event.target.name] = event.target.value
        this.setState({address})
    }

    render(){
        const visibility = this.state.display;
        const cart = this.state.cart;
        const validated = this.state.validated;
        const fetching = this.state.fetching
        return (
            <React.Fragment>
                <Container className="justify-content-md-center">
                    <Row className="basket">
                        <h3>Total Price:{((cart.apple * prices.apple + cart.orange * prices.orange) / 100).toLocaleString('en-US', {style: 'currency', currency: 'usd'})}</h3>
                        <Button variant="outline-dark" id="checkoutBtn" onClick={() => this.setState({display: !this.state.display})}>Checkout</Button>
                    </Row>
                    <Row className="products">
                        {
                            products.map(product =>
                                <Col md="auto">
                                    <Card bg="dark" text="white" className="fruit" id={product.name} key={product.id}>
                                        <Card.Img variant="top" src={product.img} alt={product.name} />
                                        <Card.Body>
                                            <Card.Title>{product.name} - {product.price}</Card.Title>
                                            <Card.Text>Quantity: {cart[product.name]}</Card.Text>
                                            <Button name={product.name} value={1} variant="outline-light" style={{width: 150}} onClick={this.handleCartChange}>+</Button>
                                            <Button name={product.name} value={-1} variant="outline-light" style={{width: 150}} onClick={this.handleCartChange}  disabled={cart[product.name] <= 0}>-</Button>
                                        </Card.Body>
                                    </Card>
                                </Col>
                            )
                        }
                    </Row>
                    <Fade in={visibility} timeout={1000}>
                        <Row className="checkout" >
                            <Card bg="dark" text="white" variant="dark">
                                <Card.Body>
                                    
                                    <Card.Title> <h1>CHECKOUT</h1></Card.Title>
                                    <Form noValidate validated={validated}  onSubmit={this.handleSubmit}>
                                        <Form.Row>
                                          <Form.Group>
                                                <div style={{width: '450px', margin: '10px',marginLeft: '140px', padding: '5px', border: '2px solid white', borderRadius: '10px'}}>
                                                    <CardElement style={{base: {fontSize: '18px', color: 'white'}}}/>
                                                </div>
                                          </Form.Group>
                                        </Form.Row>
                                        <Form.Row>
                                            <Form.Group as={Col} md="4" controlId="validationCustom01">
                                                <Form.Label>First name</Form.Label>
                                                <Form.Control name="name"
                                                    required
                                                    type="text"
                                                    placeholder="First name"
                                                    onChange={this.handleChange}
                                                />
                                                <Form.Control.Feedback type="invalid">Please Enter Your name</Form.Control.Feedback>
                                            </Form.Group>
                                            <Form.Group as={Col} md="8" controlId="validationCustom02">
                                                <Form.Label>Email</Form.Label>
                                                <Form.Control name="email" type="email" placeholder="Enter email" onChange={this.handleChange} required />
                                                <Form.Control.Feedback type="invalid">Please Enter A Valid Email</Form.Control.Feedback>
                                            </Form.Group>
                                      </Form.Row>
                                      <Form.Row>
                                            <Form.Group as={Col} md="7" controlId="validationCustom03">
                                                <Form.Label>Address</Form.Label>
                                                <Form.Control name="line1" type="text" placeholder="Address" onChange={this.handleAddressChange} required />
                                                <Form.Control.Feedback type="invalid">
                                                    Please provide a valid Address.
                                                </Form.Control.Feedback>
                                            </Form.Group>
                                            <Form.Group as={Col} md="5" controlId="validationCustom04">
                                                <Form.Label>City</Form.Label>
                                                <Form.Control name="city" type="text" placeholder="City" onChange={this.handleAddressChange} required />
                                                <Form.Control.Feedback type="invalid">
                                                    Please provide a valid city.
                                                </Form.Control.Feedback>
                                            </Form.Group>
                                      </Form.Row>
                                      <Form.Row>
                                            <Form.Group as={Col} md="5" controlId="validationCustom04">
                                                <Form.Label>Country</Form.Label>
                                                <Form.Control name="country" type="text" placeholder="Country" onChange={this.handleAddressChange} required />
                                                <Form.Control.Feedback type="invalid">
                                                    Please provide a valid country.
                                                </Form.Control.Feedback>
                                            </Form.Group>
                                            <Form.Group as={Col} md="4" controlId="validationCustom05">
                                                <Form.Label>State/Province</Form.Label>
                                                <Form.Control name="state" type="text" placeholder="State/Province" onChange={this.handleAddressChange} required />
                                                <Form.Control.Feedback type="invalid">
                                                    Please provide a valid state.
                                                </Form.Control.Feedback>
                                            </Form.Group>
                                            <Form.Group as={Col} md="3" controlId="validationCustom06">
                                                <Form.Label>Postal code</Form.Label>
                                                <Form.Control name="postal_code" type="text" placeholder="Postal code" onChange={this.handleAddressChange} required />
                                                <Form.Control.Feedback type="invalid">
                                                    Please provide a valid zip.
                                                </Form.Control.Feedback>
                                            </Form.Group>
                                        </Form.Row>
                                        {!fetching? 
                                            <Button variant="outline-light" type="submit" >Purchase</Button>
                                            :
                                            <Button variant="outline-light" type="submit" disabled='true'>Purchasing</Button>
                                        }
                                        
                                    </Form>
                                </Card.Body>
                            </Card>
                        </Row>
                        
                    </Fade>
                </Container>
            </React.Fragment>
    )
    }
}
// along with injectStripe this calls the createToken method from stripe 
// and pass it as a prop which is used to charge the user
Shop.propTypes = {
  stripe: PropTypes.shape({
    createToken: PropTypes.func.isRequired
  }).isRequired
}

export default injectStripe(Shop)